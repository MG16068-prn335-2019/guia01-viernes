/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ues.occ.edu.sv.ingenieria.prn335.control;

import java.util.ArrayList;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import ues.occ.edu.sv.ingenieria.prn335.entity.Sucursal;

/**
 *
 * @author enrique
 */
public class MotorTest {
    
    public MotorTest() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }

    /**
     * Test of modificarSucursal method, of class Motor.
     * Lanzamiento de excepcion al intentar modificar una sucursal que este activa
     */
    @Test
    public void testModificarSucursalAciva() {
        System.out.println("modificarSucursal");
        int id_sucursal = 1;
        String nombre = "Cinepolis Metrocentro SM";
        String ciudad = "San Miguel";
        String departamento = "San Miguel";
        String contacto = "Hugo Calidonio";
        boolean estado = true;
        Motor instance = new Motor();
        //Boolean result = instance.modificarSucursal(id_sucursal, nombre, ciudad, departamento, contacto, estado);
        Throwable exception= assertThrows(IllegalArgumentException.class, () ->{
        Boolean result = instance.modificarSucursal(id_sucursal-1, nombre, ciudad, departamento, contacto, estado);
        });
        assertEquals("Error! no se puede modificar una sucursal que este activa",exception.getMessage());
    }
    
    
    @Test
    public void testModificarSucursalParametroVacio() {
        System.out.println("modificarSucursal");
        int id_sucursal =0;
        String nombre = "";
        String ciudad = "";
        String departamento = "";
        String contacto = "";
        boolean estado=false ;
        Motor instance = new Motor();
        //Boolean result = instance.modificarSucursal(id_sucursal, nombre, ciudad, departamento, contacto, estado);
        Throwable exception= assertThrows(IllegalArgumentException.class, () ->{
        Boolean result = instance.modificarSucursal(id_sucursal-1, nombre, ciudad, departamento, contacto, estado);
        });
        assertEquals("Formulario incompleto, complete los datos por favor",exception.getMessage());
    }
    
    /**
     * Test of modificarSucursal method, of class Motor.
     * Comprobacion de modificacion de datos, se modifica contacto de un id y se compara el resultado
     * con el esperado que fue el que se paso como parametro 
     */
    @Test
    public void testModificarSucursalCorrecto() {
        /**
         * Sigancion de valores a las variables para la prueba
         */
        System.out.println("modificarSucursal");
        int id_sucursal = 3;//se agrega un id valido para que pase la prueba
        String nombre = "Cinepolis Metrocentro SM";
        String ciudad = "San Miguel";
        String departamento = "San Miguel";
        String contacto = "Hugo Calidonio";//Dato a modificar en la posicion solicitada
        boolean estado = true;
        Motor instance = new Motor();
        instance.modificarSucursal(id_sucursal-1, nombre, ciudad, departamento, contacto, estado);//invocacion de metodo a probar
        assertEquals("Hugo Calidonio",instance.sucursales.get(id_sucursal-1).getContacto());
        //se compara que el resultado que se espera es igual al resultado obtenido del proceso
    }
    
}
